package Logic;


import Logic.Cards.Card;

import java.util.ArrayList;

public class Debug {

	public static void print(String text) {

		System.out.println("\n" + colored(text, "yellow"));
	}

	public static void printCards(ArrayList<Card> cards) {

		for(Card card : cards) {
			System.out.print(colored(card.getRank() + "" + card.getSuit().charAt(0), "blue") + "\t\t");
		}
		System.out.println();
	}

	private static String colored(String text, String color) {
		String color_code;

		switch(color) {
			case "red":
				color_code = "\u001B[31m";
				break;

			case "green":
				color_code = "\u001B[32m";
				break;

			case "yellow":
				color_code = "\u001B[33m";
				break;

			case "blue":
				color_code = "\u001B[34m";
				break;

			case "purple":
				color_code = "\u001B[35m";
				break;

			case "cyan":
				color_code = "\u001B[36m";
				break;

			default:
				color_code = "\u001B[37m";
		}

		text = color_code + text + "\u001B[37m";

		return text;
	}
}
