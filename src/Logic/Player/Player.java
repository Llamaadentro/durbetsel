package Logic.Player;

import Logic.Cards.Card;

import java.util.ArrayList;

abstract public class Player {

	/** Имя игрока */
	private String name;

	/** Подготовленные карты */
	ArrayList<Card> preparedCards = new ArrayList<>();

	/**
	 * Конструктора класса Player
	 * @param name имя игрока
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * Абстрактный метод, возвращает instance карты из руки
	 * @param cardsOnTable карты, лежащие на столе (для валидаии хода)
	 * @param defenceCards карты, лежащие на столе (для валидаии хода)
	 * @param defenderHandSize количество карт на руке у соперника (для валидации хода)
	 * @return
	 */
	abstract public Card attack(ArrayList<Card> cardsOnTable, ArrayList<Card> defenceCards, int defenderHandSize);

	/**
	 * Абстрактный метод, возвращает одно из четырех возможных действий (defend, redirect, take)
	 * @param attackCards карты, которые нужно отбить
	 * @param attackerHandSize количество карт на руке у соперника (для валидации хода при redirect)
	 * @return
	 */
	abstract public String chooseAction(ArrayList<Card> attackCards, ArrayList<Card> defenceCards, int attackerHandSize);

	private ArrayList<Card> hand = new ArrayList<>();

	/**
	 * Добавляет карты в руку
	 * @param cards набор карт
	 */
	public void takeCards(ArrayList<Card> cards) {

		hand.addAll(cards);
	}

	/**
	 * Возвращает имя игрока
	 * @return name мя
	 */
	public String getName() {
		return name;
	}

	/**
	 * Возвращает количество карт в руке
	 * @return int количество
	 */
	public int getHandSize() {

		return hand.size();
	}

	/**
	 * Возвращает список карт в руке
	 * @return hand список карт
	 */
	public ArrayList<Card> getHand() {

		return hand;
	}

	public ArrayList<Card> getPreparedCards() {

		return preparedCards;
	}


}
