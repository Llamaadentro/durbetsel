package Logic.Player;

import Logic.Cards.Card;

import java.util.ArrayList;
import java.util.Scanner;

public class Auto extends Player {

	Scanner in = new Scanner(System.in);

	public Auto(String name) {
		super(name);
	}

	@Override
	public Card attack(ArrayList<Card> cardsOnTable, ArrayList<Card> defenceCards, int defenderHandSize) {

		if (preparedCards.size() > 0) {
			return preparedCards.get(0);
		}

		preparedCards.clear();
		sortHand();

		for (Card card : getHand()) {

			if (canAttack(card, cardsOnTable)) {
				getHand().remove(card);
				return card;
			}
		}

		return null;
	}

	private boolean canAttack(Card chosenCard, ArrayList<Card> cardsOnTable) {

		if (cardsOnTable.size() == 0) {
			return true;
		}

		for (Card card : cardsOnTable) {

			if (card.getRank() == chosenCard.getRank()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public String chooseAction(ArrayList<Card> attackCards, ArrayList<Card> defenceCards, int attackerHandSize) {

		preparedCards.clear();
		sortHand();

		for (Card card : getHand()) {

			if (canRedirect(attackCards, defenceCards, card, attackerHandSize)) {
				preparedCards.add(card);
				getHand().remove(card);
				return "redirect";
			}
		}


		for (int i = defenceCards.size(); i < attackCards.size(); i++) {

			for (Card card : getHand()) {

				if(card.getSuit().equals(attackCards.get(i).getSuit()) && card.getValue() > attackCards.get(i).getValue()) {
					preparedCards.add(card);
					getHand().remove(card);
					break;
				} else if(card.isTrump() && !attackCards.get(i).isTrump()) {
					preparedCards.add(card);
					getHand().remove(card);
					break;
				}
			}
		}

		if(preparedCards.size() == attackCards.size() - defenceCards.size()) {
			return "defend";
		} else {
			getHand().addAll(preparedCards);
			preparedCards.clear();
		}

		return "take";
	}

	private boolean canRedirect(ArrayList<Card> attackCards, ArrayList<Card> defenceCards, Card chosenCard, int attackerHandSize) {

		if (defenceCards.size() == 0 && attackCards.get(attackCards.size() - 1).getRank() == chosenCard.getRank() && attackerHandSize > attackCards.size()) {
			return true;
		}

		return false;
	}

	private void sortHand() {

		Card tempCard;

		for (int i = getHand().size() - 2; i >= 0; i--) {

			for (int j = i; j < getHand().size() - 1; j++) {

				if (getHand().get(j).getValue() > getHand().get(j + 1).getValue()) {
					tempCard = getHand().get(j);
					getHand().set(j, getHand().get(j + 1));
					getHand().set(j + 1, tempCard);
				} else {
					break;
				}
			}
		}
	}


}
