package Logic.Player;

import Logic.Cards.Card;

import java.util.ArrayList;
import java.util.Scanner;

public class Human extends Player {

	Scanner in = new Scanner(System.in);

	public Human(String name) {
		super(name);
	}

	@Override
	public Card attack(ArrayList<Card> cardsOnTable, ArrayList<Card> defenceCards, int defenderHandSize) {

		if(preparedCards.size() > 0) {
			return preparedCards.get(0);
		}

		while (true) {

			System.out.println(getName() + ": Choose card to attack (e.g.: 8 of clubs | ace of hearts):");
			String command[] = in.nextLine().split(" ");

			if (cardsOnTable.size() > 0 && command[0].equals("go")) {

				return null;
			}

			if (command.length == 3) {

				Card card = getChosenCard(command[0], command[2]);

				if (card != null) {

					if(canAttack(card, cardsOnTable)) {
						getHand().remove(card);
						return card;
					}
				}
			}
		}
	}

	private boolean canAttack(Card chosenCard, ArrayList<Card> cardsOnTable) {

		if(cardsOnTable.size() == 0) {
			return true;
		}

		for(Card card : cardsOnTable) {

			if(card.getRank() == chosenCard.getRank()) {
				return true;
			}
		}

		return false;
	}

	private boolean canDefend(ArrayList<Card> attackCards, ArrayList<Card> defenceCards, ArrayList<Card> chosenCards) {

		int counter = 0;
		for(int i = defenceCards.size(); i < attackCards.size(); i++) {

			Card attackCard = attackCards.get(i);
			Card defenceCard = chosenCards.get(counter);

			if(attackCard.getSuit().equals(defenceCard.getSuit()) && attackCard.getValue() < defenceCard.getValue() ) {

			} else if(!attackCard.isTrump() && defenceCard.isTrump()) {

			} else {
				return false;
			}


			counter++;
		}

		return true;
	}

	@Override
	public String chooseAction(ArrayList<Card> attackCards, ArrayList<Card> defenceCards, int attackerHandSize) {

		preparedCards.clear();

		while (true) {

			System.out.println(getName() + ": Defence:");
			String[] command = in.nextLine().split(" ");

			outerLoop:
			switch (command[0]) {

				case "take":
					return "take";

				case "redirect":

					if(command.length == 4) {
						Card card = getChosenCard(command[1], command[3]);
						if(card != null && canRedirect(attackCards, card, attackerHandSize)) {
							preparedCards.add(card);
							getHand().remove(card);
							return "redirect";
						}
					}

				default:
					if (command.length == (attackCards.size() - defenceCards.size()) * 3) {

						for(int i = 0; i < command.length; i = i + 3) {

							Card card = getChosenCard(command[i], command[i + 2]);

							if (card != null) {

								preparedCards.add(card);
								getHand().remove(card);
							} else {
								break outerLoop;
							}
						}

						if(canDefend(attackCards, defenceCards, preparedCards)) {
							return "defend";
						} else {
							preparedCards.clear();
						}
					}
			}
		}
	}

	private boolean canRedirect(ArrayList<Card> attackCards, Card chosenCard, int attackerHandSize) {

		if(attackCards.get(attackCards.size() - 1).getRank() == chosenCard.getRank() && attackerHandSize > attackCards.size()) {
			return true;
		}

		return false;
	}

	private Card getChosenCard(String name, String suit) {

		for (Card card : getHand()) {

			if (card.getName().equals(name) && card.getSuit().equals(suit)) {

				return card;
			}
		}

		return null;
	}


}
