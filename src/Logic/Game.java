package Logic;

import GUI.Durak;
import Logic.Cards.Card;
import Logic.Cards.Deck;
import Logic.Player.Auto;
import Logic.Player.Human;
import Logic.Player.Player;

import java.util.ArrayList;

public class Game {

	private ArrayList<Player> players;
	private int turnIndex = 0;

	private Player attacker;
	private Player defender;

	private ArrayList<Card> attackCards = new ArrayList<>();
	private ArrayList<Card> defenceCards = new ArrayList<>();

	private Deck deck;
	private Card trumpCard;

	/**
	 * Подготовка к игре
	 *
	 * @param players
	 */
	public void init(ArrayList<Player> players) {

		this.players = players;

		// Создаем и тасуем колоду, назначаем козырь
		deck = new Deck();
		deck.shuffle();
		trumpCard = deck.getTrumpCard();

		// Раздаем карты игрокам
		for (Player player : players) {

			player.takeCards(deck.getCardSet(6));
		}

		setNextPlayers();

		sendToPrint();


		while (true) {

			String winnername = checkAnyOneRanOutOfCards();
			sendToPrint();
			if (deck.getDeckSize() == 0 && !(winnername.equals("none"))) {
				if (winnername.equals("draw")){
					System.out.println("\nDRAW!");
					break;
				} else {
					System.out.println("\n"+winnername + " WINS!!!");
					break;
				}
			}

			turn();
		}
	}

	private String checkAnyOneRanOutOfCards() {
		int winnercount = 0;
		String winnername = "none";

		for (Player player : players) {
			if (player.getHandSize() == 0) {
				winnername = player.getName();
				winnercount++;
			}
		}

		if (winnercount == 1) {
			return winnername;
		} else if (winnercount>1){
			return "draw";
		}
		return "none";


	}

	public void turn() {

		Card attackCard = attacker.attack(getCardsOnTable(), defenceCards, defender.getHandSize());

		if (attackCard == null) {
			defenceCards.clear();
			attackCards.clear();

			giveCadrsToPlayers();

			setNextPlayers();
			return;
		}

		attackCards.add(attackCard);
		attacker.getPreparedCards().clear();


		sendToPrint();

		//debug();

		defenceStep();
	}

	private void giveCadrsToPlayers() {

		for (Player player : players) {
			player.takeCards(deck.getCardSet(6 - player.getHandSize()));
		}
	}

	/**
	 * Ход отбивающегося
	 */
	public void defenceStep() {

		String action = defender.chooseAction(attackCards, defenceCards, attacker.getHandSize());

		switch (action) {

			case "defend":
				// Отбить карту
				defenceCards.addAll(defender.getPreparedCards());
				defender.getPreparedCards().clear();


				break;

			case "redirect":

				setNextPlayers();

				break;

			case "take":

				defender.takeCards(getCardsOnTable());
				giveCadrsToPlayers();
				defenceCards.clear();
				attackCards.clear();

				setNextPlayers();
				setNextPlayers();

				break;
		}

		//sendToPrint();
	}

	private void setNextPlayers() {
		attacker = players.get(turnIndex % players.size());
		defender = players.get((turnIndex + 1) % players.size());
		turnIndex++;
	}

	public void debug() {

		Debug.print(deck.getDeckSize() + " cards in deck");
		Debug.print("Trump: " + deck.getTrumpCard().getSuit());

		for (Player player : players) {

			Debug.print("Player " + player.getName() + "'s cards:");
			Debug.printCards(player.getHand());
		}

		Debug.print("On table: ");
		Debug.printCards(attackCards);
		Debug.printCards(defenceCards);

	}

	public ArrayList<Card> getCardsOnTable() {

		ArrayList<Card> cardsOnTable = new ArrayList<>();

		cardsOnTable.addAll(attackCards);
		cardsOnTable.addAll(defenceCards);

		return cardsOnTable;
	}

	private Card[][] convertTable() {

		Card[][] table = new Card[2][6];

		for (int i = 0; i < attackCards.size(); i++) {

			table[0][i] = attackCards.get(i);
		}

		for (int i = 0; i < defenceCards.size(); i++) {

			table[1][i] = defenceCards.get(i);
		}

		return table;
	}

	private void sendToPrint() {

		ArrayList<Card> handComp = (attacker instanceof Auto) ? attacker.getHand() : defender.getHand();
		ArrayList<Card> handHuman = (attacker instanceof Human) ? attacker.getHand() : defender.getHand();
		Durak.main2(handComp, handHuman, convertTable(), deck);
	}
}
