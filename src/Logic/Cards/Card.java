package Logic.Cards;

public class Card {

	/** Реальное значение с учетом того, козырная карта или нет (козырь = +15 очков) */
	private int value;

	/** Номинал карты (6 - 14) */
	private int rank;

	/** Масть */
	private String suit;

	/** Козырь */
	private boolean isTrump = false;

	// печатное имя для ввода
	private String name;

	private String printName;

	private String[] printNames = {"10", " J", " Q", " K", " A"};

	/**
	 * Конструктор
	 * Создает карту, задает ей масть и номинал
	 *
	 * @param rank int
	 * @param suit String
	 */
	public Card(int rank, String suit) {

		this.rank = rank;
		this.value = rank;

		if(value < 10) {
			printName = " " + String.valueOf(rank);
		} else {
			printName = printNames[value - 10];
		}

		this.suit = suit;

		switch (this.printName) {
			case " J":
				this.name = "jack";
				break;
			case " Q":
				this.name = "queen";
				break;
			case " K":
				this.name = "king";
				break;
			case " A":
				this.name = "ace";
				break;
			default:
				this.name = String.valueOf(rank);
		}



	}

	public Card(String name, String suit){
		this.name = name;
		this.suit = suit;

		switch (this.name) {
			case "jack":
				this.rank = 11;
				break;
			case "queen":
				this.rank = 12;
				break;
			case "king":
				this.rank = 13;
				break;
			case "ace":
				this.rank = 14;
				break;
			default:
				this.rank = Integer.parseInt(name);
		}
		this.value = rank;

		if(value < 10) {
			printName = " " + String.valueOf(rank);
		} else {
			printName = printNames[value - 10];
		}


	}

	public String getName() {
		return name;
	}

	public String getPrintName() {
		return printName;
	}

	/**
	 * Устанавливает либо убирает флаг "Козырь" и пересчитывает реальное значение
	 * @param isTrump boolean
	 */
	public void setTrump(boolean isTrump) {

		this.isTrump = isTrump;
		this.value = ((isTrump) ? 1 : 0) * 15 + this.rank;
	}

	/**
	 * Возвращает номинал карты
	 * @return int
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * Возвращает реальное значение карты
	 * @return int
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Возвращает масть карты
	 * @return String
	 */
	public String getSuit() {
		return suit;
	}

	/**
	 * Проверка на козырь
	 * @return boolean
	 */
	public boolean isTrump() {
		return isTrump;
	}
}
