package Logic.Cards;

import java.util.ArrayList;
import java.util.Random;

public class Deck {

	/** Карты в колоде */
	private ArrayList<Card> cards = new ArrayList<>();

	/** Список мастей */
	private String[] suits = {"spades", "clubs", "diamonds", "hearts"};

	/** Козырная масть */
	private Card trumpCard;

	/**
	 * Конструктор
	 * Создает колоду из 36 карт
	 */
	public Deck() {

		for(int i = 6; i < 15; i++) {

			for(String suit : suits) {

				cards.add(new Card(i, suit));
			}
		}
	}

	/**
	 * Перемешивает колоду
	 */
	public void shuffle() {

		Card tempCard;
		int tempCardIndex;

		Random random = new Random();

		for(int i = 0; i < 100; i++) {

			for(int j = 0; j < cards.size(); j++) {

				tempCardIndex = random.nextInt(cards.size());

				tempCard = cards.get(j);
				cards.set(j, cards.get(tempCardIndex));
				cards.set(tempCardIndex, tempCard);
			}
		}
	}

	/**
	 * Определяет козырную масть по последней карте и проставляет каждой козырной карте колоды флаг isTrump
	 */
	public void setTrumpCard() {

		trumpCard = cards.get(cards.size() - 1);

		for(Card card : cards) {

			if(card.getSuit().equals(trumpCard.getSuit())) {

				card.setTrump(true);
			}
		}
	}

	/**
	 * Возвращает набор из N карт, удалив его из колоды
	 *
	 * @param amount int
	 * @return ArrayList
	 */
	public ArrayList<Card> getCardSet(int amount) {

		ArrayList<Card> cardSet = new ArrayList<>();

		for(int i = 0; i < amount; i++) {

			if(cards.size() == 0) {
				return cardSet;
			}
			cardSet.add(cards.get(0));
			cards.remove(0);
		}

		return cardSet;
	}

	public Card getTrumpCard() {

		if(trumpCard == null) {
			setTrumpCard();
		}

		return trumpCard;
	}

	public int getDeckSize() {

		return cards.size();
	}
}
