import Logic.Game;
import Logic.Player.Auto;
import Logic.Player.Human;
import Logic.Player.Player;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		ArrayList<Player> players = new ArrayList<>();

		players.add(new Auto("SkyNet"));
		players.add(new Human("Mankind"));

		Game game = new Game();
		game.init(players);
	}


}
