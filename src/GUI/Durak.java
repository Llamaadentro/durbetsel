package GUI;

import Logic.Cards.Card;
import Logic.Cards.Deck;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.awt.color.*;
import java.math.*;


public class Durak {

	public static void main2(ArrayList<Card> handComp, ArrayList<Card> handHuman, Card[][] Field, Deck deck) {
		clearConsole();
		printhand(handComp, false);
		printdeckandtrump(deck.getTrumpCard(), deck.getDeckSize());
		printfield(Field);
		printhand(handHuman, true);

	}

	public static void clearConsole()
	{
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
	}

//	public static void main() {
//
//		Card[] HandA = new Card[6];
//		Card[] HandComp = new Card[6]; // рука компа
//
//		Card Trumpcard = new Card();
//		String trumpdurak = Trumpcard.suit;
//
//		//Создание руки
//		for (int i = 0; i < HandA.length; i++) {
//			HandA[i] = new Card(trumpdurak);
//		}
//		for (int i = 0; i < HandComp.length; i++) {
//			HandComp[i] = new Card(trumpdurak);
//		}
//
//		//сортировка карт на руке
//		HandA = sorthand(HandA);
//		//наша рука отсортирована
//		HandComp = sorthand(HandComp);
//		// Обе руки отсортированы по мастям и возрастанию номинала
//
//		//создаем массив для игрового поля, который на каждом ходу будет наполняться отбиваемыми и бьющими картами
//		Card[][] Field = new Card[2][6];
//
//		Field[0][0] = new Card(Trumpcard.suit, 11);
//		Field[0][1] = new Card(Trumpcard.suit, 20);
//
//		//печать карты, которой походил компьютер, и его руки
//
//		//закрытая рука компа
//		printhand(HandComp, false);
//
//		// печать колоды
//		printdeckandtrump(Trumpcard, 24);
//
//
//		//печать карты, которой походил компьютер
//
//		for (int f = 0; f < Field[0].length; f++) {
//			if (Field[0][f] != null) {
//				System.out.println(ConsoleColors.GREEN_BACKGROUND + ConsoleColors.BLACK + " Computer played" + Field[0][f].name);
//			}
//		}
//		System.out.println();
//
//		// вариативный вывод поля
//
//		printfield(Field);
//
//		//Печать руки игрока
//
//		printhand(HandA, true);
//
//        /*
//        double max = 0;
//        int maxnum = 0;
//        for (int i = 0; i < HandA.length; i++) {
//            if (max <= HandA[i].innervalue){
//                max = HandA[i].innervalue;
//                maxnum = i;
//            }
//        }
//        System.out.println(ConsoleColors.GREEN_BACKGROUND+ConsoleColors.YELLOW_BOLD+"\n The highest card is"+HandA[maxnum].name);
//        */
//
//		// определение игроком карты для отбоя
//
//		String mycard = "";
//		Scanner cardname = new Scanner(System.in);
//
//		boolean checker = true;
//		int beaterposition1 = 0;
//		int beaterposition2 = 0;
//		int beaterposition3 = 0;
//		int beaterposition4 = 0;
//		int beaterposition5 = 0;
//		int beaterposition6 = 0;
//
//		while (checker) {
//			System.out.println(ConsoleColors.GREEN_BACKGROUND + ConsoleColors.YELLOW_BOLD + "\n So, what are you going to beat me with?");
//			mycard = cardname.nextLine();
//
//			for (int i = 0; i < HandA.length; i++) {
//				if ((" " + mycard).equals((HandA[i].name)) && (((HandA[i].suit.equals(Field[0][0].suit)) && (HandA[i].innervalue > Field[0][0].innervalue)) || HandA[i].istrump && !Field[0][0].istrump)) {
//					beaterposition1 = i;
//					checker = false;
//					Field[1][0] = HandA[i];
//				}
//			}
//			if (checker) {
//				System.out.println("Wrong attempt. Choose an appropriate card.");
//			}
//		}
//
//		Field[1][1] = new Card(Trumpcard.suit, 25);
//		Field[0][2] = new Card(Trumpcard.suit, 16);
//
//
//		//печать поля с отбоем
//
//		for (int k = 0; k < HandComp.length - 3; k++) {
//			System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
//			for (int i = 0; i < 7; i++) {
//				System.out.print(ConsoleColors.GREEN_BACKGROUND + "\033[4;35m" + "_");
//			}
//			System.out.print(ConsoleColors.GREEN_BACKGROUND + " ");
//		}
//		System.out.println();
//
//		for (int i = 0; i < 4; i++) {
//			for (int k = 0; k < HandComp.length - 3; k++) {
//				System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
//				for (int l = 0; l < 7; l++) {
//					System.out.print("\033[46m" + "\033[4;35m" + "Х");
//				}
//				System.out.print(ConsoleColors.GREEN_BACKGROUND + " ");
//			}
//			System.out.println();
//		}
//		System.out.println("\n");
//
//		//печать колоды c козырем
//
//		printdeckandtrump(Trumpcard, 24);
//
//		System.out.println();
//		//печать поля с отбитыми картами
//
//		printfield(Field);
//
//
//		//печать остатка руки
//		System.out.println("\n");
//		System.out.println(ConsoleColors.GREEN_BACKGROUND + ConsoleColors.BLACK + " Good! Rest of your hand:\n");
//
//		for (int i = 0; i < HandA.length - 1; i++) {
//			if (i != beaterposition1) {
//				System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA[i].suit) + HandA[i].printvalue + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + suitsymb(HandA[i].suit) + "    " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
//			}
//		}
//		System.out.println();
//		for (int i = 0; i < HandA.length - 1; i++) {
//			if (i != beaterposition1) {
//				System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA[i].suit) + " " + suitsymb(HandA[i].suit) + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "10" + "   " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
//			}
//		}
//		System.out.println();
//		for (int i = 0; i < HandA.length - 1; i++) {
//			if (i != beaterposition1) {
//				System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + "10" + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA[i].suit) + suitsymb(HandA[i].suit) + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
//			}
//		}
//		System.out.println();
//		for (int i = 0; i < HandA.length - 1; i++) {
//			if (i != beaterposition1) {
//				System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + suitsymb(HandA[i].suit) + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA[i].suit) + HandA[i].printvalue + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
//			}
//		}
//		System.out.println("\n");
//
//	}


	static char suitsymb(String suit) {
		char suitsymb = 0;
		switch (suit) {
			case "spades":
				suitsymb = (char) '\u2660';
				break;
			case "clubs":
				suitsymb = (char) '\u2663';
				break;
			case "diamonds":
				suitsymb = (char) '\u2666';
				break;
			default:
				suitsymb = (char) '♥';
		}
		return suitsymb;
	}

	static String suitcolor(String suit) {
		String color;
		if (suit.equals("spades") || suit.equals("clubs")) {
			color = ConsoleColors.WHITE_BOLD_BRIGHT;
		} else {
			color = ConsoleColors.RED_BOLD;
		}
		return color;
	}

	static void printdeckandtrump(Card Trumpcard, int decklength) {

        System.out.println(ConsoleColors.GREEN_BACKGROUND + ConsoleColors.YELLOW_BOLD + " Trump suit is " + Trumpcard.getSuit());

        if (decklength > 0) {
            System.out.println(" " + decklength + " cards left in the deck");

            //верхняя полоса
            System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
            for (int i = 0; i < 7; i++) {
                System.out.print(ConsoleColors.GREEN_BACKGROUND + "\033[4;35m" + "_");
            }
            System.out.print(ConsoleColors.GREEN_BACKGROUND + " ");

            System.out.println();

            //  колода с козырем
            System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");

            for (int l = 0; l < 7; l++) {
                System.out.print("\033[46m" + "\033[4;35m" + "Х");
            }
            System.out.print(ConsoleColors.GREEN_BACKGROUND);

            System.out.print(" " + ConsoleColors.BLACK_BACKGROUND + suitcolor(Trumpcard.getSuit()) + Trumpcard.getPrintName() + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + suitsymb(Trumpcard.getSuit()) + "    " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
            System.out.println();

            System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
            for (int l = 0; l < 7; l++) {
                System.out.print("\033[46m" + "\033[4;35m" + "Х");
            }
            System.out.print(ConsoleColors.GREEN_BACKGROUND);

            System.out.print(" " + ConsoleColors.BLACK_BACKGROUND + suitcolor(Trumpcard.getSuit()) + " " + suitsymb(Trumpcard.getSuit()) + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "10" + "   " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
            System.out.println();

            System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
            for (int l = 0; l < 7; l++) {
                System.out.print("\033[46m" + "\033[4;35m" + "Х");
            }
            System.out.print(ConsoleColors.GREEN_BACKGROUND);

            System.out.print(" " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + "10" + ConsoleColors.BLACK_BACKGROUND + suitcolor(Trumpcard.getSuit()) + suitsymb(Trumpcard.getSuit()) + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
            System.out.println();

            System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
            for (int l = 0; l < 7; l++) {
                System.out.print("\033[46m" + "\033[4;35m" + "Х");
            }
            System.out.print(ConsoleColors.GREEN_BACKGROUND);

            System.out.print(" " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + suitsymb(Trumpcard.getSuit()) + ConsoleColors.BLACK_BACKGROUND + suitcolor(Trumpcard.getSuit()) + Trumpcard.getPrintName() + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
            System.out.println("\n");

        } else {
            System.out.println(" No deck cards left.");
            System.out.println(ConsoleColors.GREEN_BACKGROUND);
            System.out.println(ConsoleColors.GREEN_BACKGROUND);
            System.out.println(ConsoleColors.GREEN_BACKGROUND);
            System.out.println(ConsoleColors.GREEN_BACKGROUND);
            System.out.println(ConsoleColors.GREEN_BACKGROUND + "\n");
        }
    }


	//печать руки взакрытую
	static void printhand(ArrayList<Card> HandA, boolean open) {
		if (open) {
			System.out.println(ConsoleColors.GREEN_BACKGROUND + ConsoleColors.BLACK);

			for (int i = 0; i < HandA.size(); i++) {
				if (HandA.get(i) != null) {

					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA.get(i).getSuit()) + HandA.get(i).getPrintName() + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + suitsymb(HandA.get(i).getSuit()) + "    " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				}
			}
			System.out.println();
			for (int i = 0; i < HandA.size(); i++) {
				if (HandA.get(i) != null) {

					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA.get(i).getSuit()) + " " + suitsymb(HandA.get(i).getSuit()) + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "10" + "   " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				}
			}
			System.out.println();
			for (int i = 0; i < HandA.size(); i++) {
				if (HandA.get(i) != null) {

					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + "10" + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA.get(i).getSuit()) + suitsymb(HandA.get(i).getSuit()) + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				}
			}
			System.out.println();
			for (int i = 0; i < HandA.size(); i++) {
				if (HandA.get(i) != null) {

					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + suitsymb(HandA.get(i).getSuit()) + ConsoleColors.BLACK_BACKGROUND + suitcolor(HandA.get(i).getSuit()) + HandA.get(i).getPrintName() + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				}
			}
			System.out.println();
		} else {

			System.out.println(ConsoleColors.GREEN_BACKGROUND + ConsoleColors.BLACK);

			for (int k = 0; k < HandA.size(); k++) {
				if (HandA.get(k) != null) {
					System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
					for (int i = 0; i < 7; i++) {
						System.out.print(ConsoleColors.GREEN_BACKGROUND + "\033[4;35m" + "_");
					}
					System.out.print(ConsoleColors.GREEN_BACKGROUND + " ");
				}
			}
			System.out.println();

			for (int i = 0; i < 4; i++) {
				for (int k = 0; k < HandA.size(); k++) {
					if (HandA.get(k) != null) {
						System.out.print(ConsoleColors.GREEN_BACKGROUND + "  ");
						for (int l = 0; l < 7; l++) {
							System.out.print("\033[46m" + "\033[4;35m" + "Х");
						}
						System.out.print(ConsoleColors.GREEN_BACKGROUND + " ");
					}
				}
				System.out.println();
			}
			System.out.println();

		}
	}


	//сортировка любой руки
//	static Card[] sorthand(Card[] Hand) {
//
//		double rank[][] = new double[2][Hand.length];
//
//		for (int k = 0; k < Hand.length; k++) {
//			rank[0][k] = (double) k;
//		}
//
//		for (int i = 0; i < Hand.length; i++) {
//			if (Hand[i].istrump) {
//				rank[1][i] = Hand[i].sortvalue + 50;
//			} else {
//				rank[1][i] = Hand[i].sortvalue;
//			}
//		}
//
//		for (int j = 0; j < rank[1].length; j++) {
//			for (int l = j + 1; l < rank[1].length; l++) {
//				if (rank[1][j] > rank[1][l]) {
//					double tmp = rank[1][j];
//					double tmpnum = rank[0][j];
//					rank[1][j] = rank[1][l];
//					rank[0][j] = rank[0][l];
//					rank[1][l] = tmp;
//					rank[0][l] = tmpnum;
//				}
//			}
//		}
//
//		Card[] Handranked = new Card[Hand.length];
//
//		for (int i = 0; i < Handranked.length; i++) {
//			Handranked[i] = Hand[(int) rank[0][i]];
//		}
//
//		return Handranked;
//	}

	static void printfield(Card[][] Field) {

		for (int f = 0; f < Field[0].length; f++) {

			if (Field[0][f] != null) {
				if (Field[1][f] == null) {
					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[0][f].getSuit()) + Field[0][f].getPrintName() + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + suitsymb(Field[0][f].getSuit()) + "    " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				} else {
					System.out.print("  " + ConsoleColors.CYAN_BACKGROUND + suitcolor(Field[0][f].getSuit()) + Field[0][f].getPrintName() + ConsoleColors.CYAN_BACKGROUND + ConsoleColors.CYAN_BOLD + suitsymb(Field[0][f].getSuit()) + "    " + ConsoleColors.GREEN + ConsoleColors.GREEN_BACKGROUND + suitsymb(Field[0][f].getSuit()) + "0" + "  ");

				}
			}
		}
		System.out.println();

		for (int f = 0; f < Field[0].length; f++) {

			if (Field[0][f] != null) {
				if (Field[1][f] == null) {
					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[0][f].getSuit()) + " " + suitsymb(Field[0][f].getSuit()) + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "10" + "   " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				} else {
					System.out.print("  " + ConsoleColors.CYAN_BOLD + ConsoleColors.CYAN_BACKGROUND + 1 + suitcolor(Field[0][f].getSuit()) + ConsoleColors.CYAN_BACKGROUND + suitsymb(Field[0][f].getSuit()) + ConsoleColors.CYAN_BOLD + ConsoleColors.CYAN_BACKGROUND + 1 + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[1][f].getSuit()) + Field[1][f].getPrintName() + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + suitsymb(Field[1][f].getSuit()) + "    " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");

				}
			}
		}
		System.out.println();

		for (int f = 0; f < Field[0].length; f++) {

			if (Field[0][f] != null) {
				if (Field[1][f] == null) {
					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + "10" + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[0][f].getSuit()) + suitsymb(Field[0][f].getSuit()) + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				} else {
					System.out.print("  " + ConsoleColors.CYAN_BOLD + ConsoleColors.CYAN_BACKGROUND + suitsymb(Field[1][f].getSuit()) + 11 + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[1][f].getSuit()) + " " + suitsymb(Field[1][f].getSuit()) + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "10" + "   " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");

				}
			}
		}
		System.out.println();

		for (int f = 0; f < Field[0].length; f++) {

			if (Field[0][f] != null) {
				if (Field[1][f] == null) {
					System.out.print("  " + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + suitsymb(Field[0][f].getSuit()) + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[0][f].getSuit()) + Field[0][f].getPrintName() + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				} else {
					System.out.print("  " + ConsoleColors.CYAN_BOLD + ConsoleColors.CYAN_BACKGROUND + suitsymb(Field[1][f].getSuit()) + 11 + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + "10" + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[1][f].getSuit()) + suitsymb(Field[1][f].getSuit()) + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");

				}
			}
		}
		System.out.println();

		for (int f = 0; f < Field[0].length; f++) {

			if (Field[0][f] != null) {
				if (Field[1][f] == null) {
					System.out.print("  " + ConsoleColors.GREEN_BACKGROUND);
				} else {
					System.out.print("  " + ConsoleColors.GREEN + ConsoleColors.GREEN_BACKGROUND + "0" + suitsymb(Field[1][f].getSuit()) + 1 + ConsoleColors.BLACK_BACKGROUND + ConsoleColors.BLACK_BOLD + "   " + suitsymb(Field[1][f].getSuit()) + ConsoleColors.BLACK_BACKGROUND + suitcolor(Field[1][f].getSuit()) + Field[1][f].getPrintName() + " " + ConsoleColors.WHITE + ConsoleColors.GREEN_BACKGROUND + " ");
				}
			}
		}
		System.out.println();

	}


}


//class Card1 {
//	String name;
//	int sortvalue;
//	double innervalue;
//	String printvalue;
//	String suit;
//	boolean istrump;
//
//
//	Card(String trumpdurak) {
//		sortvalue = (int) Math.round((Math.random() * 35));
//		//Масть
//		switch ((int) sortvalue / 9) {
//			case 0:
//				suit = "spades";
//				break;
//			case 1:
//				suit = "clubs";
//				break;
//			case 2:
//				suit = "diamonds";
//				break;
//			default:
//				suit = "hearts";
//		}
//		//Козырность
//		if (trumpdurak.equals(suit)) {
//			istrump = true;
//		}
//		//Значение для сравнения при отбое
//		innervalue = sortvalue % 9;
//
//		//Индекс для печати
//		if ((sortvalue % 9) < 4) {
//			printvalue = " " + Integer.toString((int) (sortvalue % 9) + 6);
//		} else if ((sortvalue % 9) == 4) {
//			printvalue = "10";
//		} else if ((sortvalue % 9) == 5) {
//			printvalue = " J";
//		} else if ((sortvalue % 9) == 6) {
//			printvalue = " Q";
//		} else if ((sortvalue % 9) == 7) {
//			printvalue = " K";
//		} else {
//			printvalue = " A";
//		}
//
//		switch (printvalue) {
//			case " J":
//				name = " jack of " + suit;
//				break;
//			case " Q":
//				name = " queen of " + suit;
//				break;
//			case " K":
//				name = " king of " + suit;
//				break;
//			case " A":
//				name = " ace of " + suit;
//				break;
//			case "10":
//				name = " 10 of " + suit;
//				break;
//			default:
//				name = printvalue + " of " + suit;
//		}
//
//
//	}
//
//	//конструктор для открытого на старте козыря без параметров - по умолчанию козырная карта
//	Card() {
//		sortvalue = (int) Math.round((Math.random() * 35));
//		//Масть
//		switch ((int) sortvalue / 9) {
//			case 0:
//				suit = "spades";
//				break;
//			case 1:
//				suit = "clubs";
//				break;
//			case 2:
//				suit = "diamonds";
//				break;
//			default:
//				suit = "hearts";
//		}
//		//Козырность
//
//		istrump = true;
//
//		//Значение для сравнения при отбое
//		innervalue = sortvalue % 9;
//
//		//Индекс для печати
//		if ((sortvalue % 9) < 4) {
//			printvalue = " " + Integer.toString((int) (sortvalue % 9) + 6);
//		} else if ((sortvalue % 9) == 4) {
//			printvalue = "10";
//		} else if ((sortvalue % 9) == 5) {
//			printvalue = " J";
//		} else if ((sortvalue % 9) == 6) {
//			printvalue = " Q";
//		} else if ((sortvalue % 9) == 7) {
//			printvalue = " K";
//		} else {
//			printvalue = " A";
//		}
//
//		switch (printvalue) {
//			case " J":
//				name = " jack of " + suit;
//				break;
//			case " Q":
//				name = " queen of " + suit;
//				break;
//			case " K":
//				name = " king of " + suit;
//				break;
//			case " A":
//				name = " ace of " + suit;
//				break;
//			case "10":
//				name = " 10 of " + suit;
//				break;
//			default:
//				name = printvalue + " of " + suit;
//		}
//
//
//	}
//
//	// конструктор для создания конкретной карты
//	Card(String trumpdurak, int determinedsortvalue) {
//		sortvalue = determinedsortvalue;
//
//		//Масть
//		switch ((int) sortvalue / 9) {
//			case 0:
//				suit = "spades";
//				break;
//			case 1:
//				suit = "clubs";
//				break;
//			case 2:
//				suit = "diamonds";
//				break;
//			default:
//				suit = "hearts";
//		}
//		//Козырность
//		if (trumpdurak.equals(suit)) {
//			istrump = true;
//		}
//		//Значение для сравнения при отбое
//		innervalue = sortvalue % 9;
//
//		//Индекс для печати
//		if ((sortvalue % 9) < 4) {
//			printvalue = " " + Integer.toString((int) (sortvalue % 9) + 6);
//		} else if ((sortvalue % 9) == 4) {
//			printvalue = "10";
//		} else if ((sortvalue % 9) == 5) {
//			printvalue = " J";
//		} else if ((sortvalue % 9) == 6) {
//			printvalue = " Q";
//		} else if ((sortvalue % 9) == 7) {
//			printvalue = " K";
//		} else {
//			printvalue = " A";
//		}
//
//		switch (printvalue) {
//			case " J":
//				name = " jack of " + suit;
//				break;
//			case " Q":
//				name = " queen of " + suit;
//				break;
//			case " K":
//				name = " king of " + suit;
//				break;
//			case " A":
//				name = " ace of " + suit;
//				break;
//			case "10":
//				name = " 10 of " + suit;
//				break;
//			default:
//				name = printvalue + " of " + suit;
//		}
//
//
//	}
//
//
//}


class ConsoleColors {
	// Reset
	public static final String RESET = "\033[0m";  // Text Reset

	// Regular Colors
	public static final String BLACK = "\033[0;30m";   // BLACK
	public static final String RED = "\033[0;31m";     // RED
	public static final String GREEN = "\033[0;32m";   // GREEN
	public static final String YELLOW = "\033[0;33m";  // YELLOW
	public static final String BLUE = "\033[0;34m";    // BLUE
	public static final String PURPLE = "\033[0;35m";  // PURPLE
	public static final String WHITE = "\033[0;37m";   // WHITE

	// Bold
	public static final String BLACK_BOLD = "\033[1;30m";  // BLACK
	public static final String RED_BOLD = "\033[1;31m";    // RED
	public static final String GREEN_BOLD = "\033[1;32m";  // GREEN
	public static final String YELLOW_BOLD = "\033[1;33m"; // YELLOW
	public static final String BLUE_BOLD = "\033[1;34m";   // BLUE
	public static final String PURPLE_BOLD = "\033[1;35m"; // PURPLE
	public static final String CYAN_BOLD = "\033[1;36m";   // CYAN
	public static final String WHITE_BOLD = "\033[1;37m";  // WHITE

	// Underline
	public static final String BLACK_UNDERLINED = "\033[4;30m";  // BLACK
	public static final String RED_UNDERLINED = "\033[4;31m";    // RED
	public static final String GREEN_UNDERLINED = "\033[4;32m";  // GREEN
	public static final String YELLOW_UNDERLINED = "\033[4;33m"; // YELLOW
	public static final String BLUE_UNDERLINED = "\033[4;34m";   // BLUE
	public static final String PURPLE_UNDERLINED = "\033[4;35m"; // PURPLE
	public static final String CYAN_UNDERLINED = "\033[4;36m";   // CYAN
	public static final String WHITE_UNDERLINED = "\033[4;37m";  // WHITE

	// Background
	public static final String BLACK_BACKGROUND = "\033[40m";  // BLACK
	public static final String RED_BACKGROUND = "\033[41m";    // RED
	public static final String GREEN_BACKGROUND = "\033[48m";  // GREEN
	public static final String YELLOW_BACKGROUND = "\033[43m"; // YELLOW
	public static final String BLUE_BACKGROUND = "\033[44m";   // BLUE
	public static final String PURPLE_BACKGROUND = "\033[45m"; // PURPLE
	public static final String CYAN_BACKGROUND = "\033[46m";   // CYAN
	public static final String WHITE_BACKGROUND = "\033[47m";  // WHITE

	// High Intensity
	public static final String BLACK_BRIGHT = "\033[0;90m";  // BLACK
	public static final String RED_BRIGHT = "\033[0;91m";    // RED
	public static final String GREEN_BRIGHT = "\033[0;92m";  // GREEN
	public static final String YELLOW_BRIGHT = "\033[0;93m"; // YELLOW
	public static final String BLUE_BRIGHT = "\033[0;94m";   // BLUE
	public static final String PURPLE_BRIGHT = "\033[0;95m"; // PURPLE
	public static final String CYAN_BRIGHT = "\033[0;96m";   // CYAN
	public static final String WHITE_BRIGHT = "\033[0;97m";  // WHITE

	// Bold High Intensity
	public static final String BLACK_BOLD_BRIGHT = "\033[1;90m"; // BLACK
	public static final String RED_BOLD_BRIGHT = "\033[1;91m";   // RED
	public static final String GREEN_BOLD_BRIGHT = "\033[1;92m"; // GREEN
	public static final String YELLOW_BOLD_BRIGHT = "\033[1;93m";// YELLOW
	public static final String BLUE_BOLD_BRIGHT = "\033[1;94m";  // BLUE
	public static final String PURPLE_BOLD_BRIGHT = "\033[1;95m";// PURPLE
	public static final String CYAN_BOLD_BRIGHT = "\033[1;96m";  // CYAN
	public static final String WHITE_BOLD_BRIGHT = "\033[1;97m"; // WHITE

	// High Intensity backgrounds
	public static final String BLACK_BACKGROUND_BRIGHT = "\033[0;100m";// BLACK
	public static final String RED_BACKGROUND_BRIGHT = "\033[0;101m";// RED
	public static final String GREEN_BACKGROUND_BRIGHT = "\033[0;102m";// GREEN
	public static final String YELLOW_BACKGROUND_BRIGHT = "\033[0;103m";// YELLOW
	public static final String BLUE_BACKGROUND_BRIGHT = "\033[0;104m";// BLUE
	public static final String PURPLE_BACKGROUND_BRIGHT = "\033[0;105m"; // PURPLE
	public static final String CYAN_BACKGROUND_BRIGHT = "\033[0;106m";  // CYAN
	public static final String WHITE_BACKGROUND_BRIGHT = "\033[0;107m";   // WHITE

}